package com.lazday.membership.activity.list

import java.io.Serializable

data class ListResponse(
    val items: List<ListItem>,
)

data class ListItem(
    val snippet: ItemSnippet,
) : Serializable

data class ItemSnippet(
    val title: String? = "",
    val thumbnails: SnippetThumbnails,
    val channelTitle: String? = "",
    val resourceId: SnippetResourceId,
) : Serializable

data class SnippetThumbnails(
    val default: ThumbnailsDefault,
) : Serializable

data class ThumbnailsDefault(
    val url: String? = "",
) : Serializable

data class SnippetResourceId(
    val videoId: String? = "",
) : Serializable