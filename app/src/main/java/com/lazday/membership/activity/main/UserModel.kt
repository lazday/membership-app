package com.lazday.membership.activity.main

data class UserResponse(
    val range: String,
    val majorDimension: String,
    val values: List<List<String>>
)