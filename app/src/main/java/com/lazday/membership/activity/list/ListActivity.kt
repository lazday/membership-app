package com.lazday.membership.activity.list

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.button.MaterialButton
import com.lazday.membership.R
import com.lazday.membership.activity.detail.DetailActivity
import com.lazday.membership.databinding.ActivityListBinding
import com.lazday.membership.network.ApiClient

class ListActivity : AppCompatActivity() , ListView {

    private val binding by lazy { ActivityListBinding.inflate(layoutInflater) }
    private lateinit var presenter: ListPresenter
    private lateinit var lsitAdapter: ListAdapter
    private var isFlutter: Boolean = false
    private var isClose: Int = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        supportActionBar?.hide()

        presenter = ListPresenter( this,
//            PrefManager(requireContext()),
            ApiClient.getService()
        )

        lsitAdapter = ListAdapter(arrayListOf(), object: ListAdapter.AdapterListener {
            override fun onClick(item: ListItem) {
                startActivity(
                    Intent(this@ListActivity, DetailActivity::class.java)
                        .putExtra("snippet", item)
                )
            }
        })
        binding.listCourse.adapter = lsitAdapter

        presenter.fetchList(isFlutter)
        setButton()

        binding.swipe.setOnRefreshListener {
            presenter.fetchList(isFlutter)
        }

        binding.btnLaravel.setOnClickListener {
            isFlutter = false
            presenter.fetchList(isFlutter)
            setButton()
        }
        binding.btnFlutter.setOnClickListener {
            isFlutter = true
            presenter.fetchList(isFlutter)
            setButton()
        }

    }

    override fun onBackPressed() {
        if (isClose == 2) {

            isClose = 1
            Handler(Looper.myLooper()!!).postDelayed({
                isClose = 2
            }, 5000)

            Toast.makeText(
                this,
                "Tap sekali lagi untuk menutup Aplikasi",
                Toast.LENGTH_SHORT
            ).show()

        } else {
            super.onBackPressed()
        }
    }

    private fun setButton() {

        val setGray = ContextCompat.getColorStateList(this, android.R.color.darker_gray)
        val setPurple = ContextCompat.getColorStateList(this, R.color.purple_500)

        listOf<MaterialButton>(
            binding.btnLaravel,
            binding.btnFlutter,
        ).forEach {
            it.setTextColor(setGray)
        }

        if (isFlutter) {
            binding.btnFlutter.setTextColor(setPurple)
        } else {
            binding.btnLaravel.setTextColor(setPurple)
        }
    }

    override fun listLoading(loading: Boolean) {
        binding.swipe.isRefreshing = loading
    }

    override fun listResponse(response: ListResponse) {
        Log.e("ListFragment", "listResponse: response ${response.toString()}")
        lsitAdapter.addList( response.items )
    }

    override fun listError(msg: String) {

    }
}