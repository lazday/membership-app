package com.lazday.membership.network

import com.lazday.membership.activity.list.ListResponse
import com.lazday.membership.activity.main.UserResponse
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @GET("users.php")
    suspend fun allUser() : Response<UserResponse>

    @GET("courses/flutter.php")
    suspend fun courseFlutter() : Response<ListResponse>

    @GET("courses/laravel.php")
    suspend fun courseLaravel() : Response<ListResponse>
}