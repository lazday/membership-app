package com.lazday.membership.activity.main

import com.lazday.membership.network.ApiService
import com.lazday.membership.preferences.PrefService
import kotlinx.coroutines.*

class MainPresenter (
    private val view: MainView,
    private val pref: PrefService,
    private val api: ApiService,
) {

    init {
        fetchUsers()
        checkLogin()
    }

    fun fetchUsers() {
        view.userLoading(true)
        GlobalScope.launch {
            try {
                val response = api.allUser()
                if (response.isSuccessful) {
                    withContext(Dispatchers.Main) {
                        view.userResponse( response.body()!! )
                        view.userLoading(false)
                    }
                } else {
                    view.userError("Terjadi kesalahan")
                }
            } catch (e: Exception) {
                view.userError(e.localizedMessage)
            }
        }
    }

    fun saveLogin(){
        pref.put("pref_is_login", true)
    }

    private fun checkLogin(){
        view.isLogin(pref.getBoolean("pref_is_login"))
    }
}

interface MainView {
//    fun setupListener()
//    fun user(user: LoginData)
    fun userLoading(loading: Boolean)
    fun userResponse(response: UserResponse)
    fun userError(msg: String)
    fun isLogin(login: Boolean)
}