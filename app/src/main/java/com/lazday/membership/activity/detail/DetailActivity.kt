package com.lazday.membership.activity.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.lazday.membership.activity.list.ListItem
import com.lazday.membership.databinding.ActivityDetailBinding
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener

class DetailActivity : AppCompatActivity() {

    private val binding by lazy { ActivityDetailBinding.inflate(layoutInflater) }
    lateinit var item: ListItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        supportActionBar?.hide()

        item = (intent.getSerializableExtra("snippet") as ListItem)
        isLoading(true)

        lifecycle.addObserver(binding.videoYoutube)
        binding.videoYoutube.apply {
            addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
                override fun onReady(youTubePlayer: YouTubePlayer) {
                    super.onReady(youTubePlayer)
                    isLoading(false)
//                    youTubePlayer.loadVideo( item.snippet.resourceId.videoId!! , 0F)
                    youTubePlayer.cueVideo( item.snippet.resourceId.videoId!! , 0F)
                }
            })
            enterFullScreen()
        }

        binding.swipe.setOnRefreshListener {
            isLoading(false)
        }
    }

    private fun isLoading(loading: Boolean) {
        binding.swipe.isRefreshing = loading
        binding.videoYoutube.visibility = if (loading) View.GONE else View.VISIBLE
    }
}