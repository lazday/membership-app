package com.lazday.membership.activity.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.lazday.membership.activity.list.ListActivity
import com.lazday.membership.databinding.ActivityMainBinding
import com.lazday.membership.network.ApiClient
import com.lazday.membership.preferences.PrefService

class MainActivity : AppCompatActivity(), MainView {

    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private lateinit var presenter: MainPresenter
    private var allUsers: List<List<String>> = listOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        presenter = MainPresenter( this,
            PrefService(this),
            ApiClient.getService()
        )

        supportActionBar?.hide()

        binding.btnUser.setOnClickListener {
            if (binding.editUser.text.toString().isNotEmpty()) {
                checkUser(binding.editUser.text.toString().trim())
            }
        }

        binding.swipe.setOnRefreshListener {
            presenter.fetchUsers()
        }

    }

    private fun checkUser(input: String){

        var isUser: Boolean = false
        allUsers.forEach {  list ->
//            Log.e("MainActivity", "checkUser: list ${list.toString()}")
            if (list[2] == input || list[3] == input) {
                Log.e("MainActivity", "checkUser: list ${list.toString()}")
                if (list.toString().contains("lunas")) isUser = true
            }
        }
        Log.e("MainActivity", "checkUser: isUser ${isUser.toString()}")

        if (isUser) {
            presenter.saveLogin()
            goToCourse()
        } else {
            Toast.makeText(
                this,
                "Data tidak ditemukan.",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    override fun userLoading(loading: Boolean) {
        binding.swipe.isRefreshing = loading
        binding.btnUser.isEnabled = !loading
    }

    override fun userResponse(response: UserResponse) {
        Log.e("MainActivity", "userResponse: response ${response.toString()}")
        allUsers = response.values
    }

    override fun userError(msg: String) {
        this.runOnUiThread(Runnable {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
            binding.swipe.isRefreshing = false
            binding.btnUser.isEnabled = false
        })
    }

    override fun isLogin(login: Boolean) {
        if (login) goToCourse()
    }

    private fun goToCourse(){
        startActivity(Intent(this, ListActivity::class.java))
        finish()
    }
}