package com.lazday.membership.activity.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lazday.membership.databinding.AdapterListBinding
import com.lazday.membership.util.loadImage

class ListAdapter (
    var courses: ArrayList<ListItem>,
    var listener: AdapterListener
): RecyclerView.Adapter<ListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            AdapterListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount() = courses.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val course = courses[position]
        holder.binding.textTitle.text = course.snippet.title
        holder.binding.textMentor.text = course.snippet.channelTitle
        loadImage(holder.binding.imageThumbnail, course.snippet.thumbnails.default.url!!)
        holder.itemView.setOnClickListener {
            listener.onClick( course )
        }
    }

    class ViewHolder(val binding: AdapterListBinding): RecyclerView.ViewHolder(binding.root)

    fun addList(list: List<ListItem>) {
        courses.clear()
        courses.addAll(list)
        notifyDataSetChanged()
    }

    interface AdapterListener {
        fun onClick(item: ListItem)
    }

}