package com.lazday.membership.activity.list

import com.lazday.membership.network.ApiService
import kotlinx.coroutines.*

class ListPresenter (
    private val view: ListView,
//    private val pref: PrefManager,
    private val api: ApiService,
) {

    init {
//        view.setupListener()
//        view.user( userLogin( pref ) )
//        fetchList()
    }

    fun fetchList(isFlutter: Boolean) {
        view.listLoading(true)
        GlobalScope.launch {
            val response = if (isFlutter) api.courseFlutter() else api.courseLaravel()
            if (response.isSuccessful) {
                withContext(Dispatchers.Main) {
                    view.listResponse( response.body()!! )
                    view.listLoading(false)
                }
            } else {
                view.listError("Terjadi kesalahan")
            }
        }
    }
}

interface ListView {
//    fun setupListener()
//    fun user(user: LoginData)
    fun listLoading(loading: Boolean)
    fun listResponse(response: ListResponse)
    fun listError(msg: String)
}