package com.lazday.membership

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate

class MembershipApp: Application() {

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate
            .setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }
}